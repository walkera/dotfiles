function realpath {
    python -c "import os; import sys; print os.path.normpath(\"$1\")"
}

function platform {
    python -c 'import sys; print sys.platform'
}

PWD=`pwd`
PTH=`dirname $0`
PLATFORM=`platform`
BASEPATH=`realpath $PWD/$PTH`
ln -s -n -f $BASEPATH/vim ~/.vim
ln -s -n -f $BASEPATH/bash ~/.bashrc.d


if [[ $PLATFORM != "darwin" ]];
then
    cp /etc/skel/.bashrc ~/.bashrc
    cat $BASEPATH/bash/bashplugins >> ~/.bashrc
    sudo apt-get install exuberant-ctags
fi

ln -s -f ~/.vim/vimrc ~/.vimrc
ln -s -f $BASEPATH/tmux/tmux.conf ~/.tmux.conf
ln -s -f $BASEPATH/git/gitconfig ~/.gitconfig
ln -s -f $BASEPATH/git/gitignore ~/.gitignore

git submodule init
git submodule update
git submodule foreach git submodule init
git submodule foreach git submodule update

function encrypt {
    FNAME=$1
    rm -f $FNAME.asc
    gpg -c -a $FNAME
    errcode=$?
    if [[ $errcode != 0 ]]; then
        return $errcode
    fi
    rm $FNAME
}

function decrypt {
    FNAME=$1
    gpg -d $FNAME 2> /dev/null
}
